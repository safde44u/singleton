public class MySingleton
{
    package singleton;

    public class MySingleton {

        private static MySingleton INSTANCE;


        private MySingleton() {

        }

        public static MySingleton getINSTANCE() {
            if (INSTANCE == null) {
                INSTANCE = new MySingleton();
            }
            return INSTANCE;
        }

        public void doUsefulStuff() {
            System.out.println("something usefull");
        }
    }

}
