package org.example;

public class MySingletonMain
{
    package singleton;

    public class MySingletonMain {

        public static void main(String[] args) {
            MySingleton mySingleton = MySingleton.getINSTANCE();

            System.out.println(mySingleton);
            System.out.println(MySingleton.getINSTANCE());
        }
    }
}
